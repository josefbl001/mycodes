/**
 * @file Interface.h
 * @author José F. (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2022-10-03
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef Interface_h
#define Interface_h
#include <Arduino.h>

class Interface{
    public:
      void begin();
    private:
    protected:
      String _ip;             // String ip address
      String _gateway;        // String gateway address
      String _netmask;        // String subred address
      String _dns;            // String dns address

      String bat_a1;  // set min battery a
      String bat_a2;  // set max battery a
      String bat_b1;  // set min battery b
      String bat_b2;  // set max battery b

      String s;       // for store string from serial port
      String s2;      // for store string from serial port
      String s3;      // for store string from serial port

      void split_input(int option_select);
      void screen0();       // screen 0 for welcome
      void ip_screen();     // screen 1 for ip assignment
      void gt_screen();     // screen 2 for gateway assignment
      void net_screen();    // screen 3 for netmask assignment
      void dns_screen();    // screen 4 for dns assignment
      void bat_screen();    // screen 5 for battery protection
};

#endif