/**
 * @file Interface.cpp
 * @author Jose F (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2022-10-03
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "Interface.h"
#include "WString.h"

// to invoke library
void Interface::begin(){
  screen0();
}

void Interface::screen0(){
  Serial.println(F("Bienvenido a la configuración del Cix Telemetry Device"));
  Serial.println(F("1- Ip."));
  Serial.println(F("2- Gateway"));
  Serial.println(F("3- Netmask"));
  Serial.println(F("4- Dns"));
  Serial.println(F("5- Voltmeter A"));
  Serial.println(F("6- Voltmeter B"));
  Serial.printnl(F("Escribe un número de la lista y presiona enter"));
  while (Serial.available() >= 0){
    if ( Serial.available() > 4 ){
      s = Serial.readStringUntil('\n');
      s.trim();
      Serial.flush();
    }
  }
  split_input(int(s))
}



void Interface::ip_screen(){
  Serial.println(F("Ingresa tu dirección IP"));
  while (Serial.available() >= 0){
    if ( Serial.available() > 4 ){
      _ip = Serial.readStringUntil('\n');
      s.trim();
      Serial.flush();
    }
  }
  delay(100);
  for (int i=0;i<4;i++){
    Serial.print(".");
  }
}

void Interface::gt_screen(){
  Serial.println(F("Ingresa tu dirección Gateway"));
  while (Serial.available() >= 0){
    if ( Serial.available() > 4 ){
      _gateway = Serial.readStringUntil('\n');
      s.trim();
      Serial.flush();
    }
  }
  delay(100);
  for (int i=0;i<4;i++){
    Serial.print(".");
  }
}