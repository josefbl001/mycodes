/**
 * @file voltmeter.cpp
 * @author José F (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2022-09-28
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "voltmeter.h"

/**
 * @brief 
 * 
 * @param analog_pin 
 * @return float 
 */
float voltmeter::get_volt(byte analog_pin){
    //Haremos esto unas 50 verces
    for (int i=0; i < _numReadings ; i++) {
        // restar la última lectura:
        _total = _total - _readings[_readIndex];
        // Leer desde el sensor
        _readings[_readIndex] = analogRead(analog_pin);
        // añadir la lectura al total:
        _total = _total + _readings[_readIndex];
        // avanzar a la siguiente posición en el array:
        _readIndex = _readIndex + 1;
        // si estamos al final del array...
        if (_readIndex >= _numReadings) {
            // ...reiniciamos el indice:
            _readIndex = 0;
        }
    }
    // calculamos el promedio:
    _average = (_total / _numReadings);
    _voltage = _average * (_vref / 1023.0);
    _vbat    = _voltage/_factor_division;

    return _vbat;
}