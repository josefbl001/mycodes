/**
 * @file main.cpp
 * @author José F. (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2022-09-14
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <Arduino.h>
#include <Wire.h>
#include "voltmeter.h"
#include "Interface.h"

const byte led_debug  = 4;  // led for debug
const byte configping = 3;  // pin for access to config mode. Here is a jumper

// to send message to i2c bus
String data_send;     // send to i2c bus
byte indicator;       // instruction scope

String ip       = "";
String gateway  = "";
String netmask  = "";
String dns      = "";

float min_a = 10.80;  // set point min, battery A
float max_a = 14.00;  // set point max, battery A
float min_b = 10.80;  // set point min, battery B
float max_b = 14.00;  // set point max, battery B

String protec_1a;   // set point min battery A. For send to i2c bus
String protec_2a;   // set point max battery A. For send to i2c bus
String protec_1b;   // set point min battery B. For send to i2c bus
String protec_2b;   // set point max battery B. For send to i2c bus

//byte relay1_a = 9;      // relay battery A pin. Charge
//byte relay1_b = 10;     // relay battery B pin. Charge
//byte relay2_a = 11;     // relay battery A pin. Discharge
//byte relay2_b = 12;     // relay battery B pin. Discharge
byte relays[] = {9, 10, 11, 12}; // arrays for relays pins

float R1                = 216000.00;    // resistencia r1
float R2                = 9870.00;      // resistencia r2
float factor_division   = (R2/(R1+R2)); // factor de división del divisor de tensión entre r1 y r2
float voltage           = 0;            // varibale para almacenar el valor calculado de la etension
float vbat              = 0;            // voltaje final de la batería despues de pasar por el voltimetro
float vref              = 4.31;         // referencia externa
int readIndex = 0;              // el índice de la lectura actual
const int numReadings = 50;     // número de muestras de lectura - aumentar para un mayor suavizado
int readings[numReadings];      // la matriz para las lecturas de la entrada analógica
unsigned long total = 0;        // Inicializamos el total
int   average = 0;
#define alpha 1.5

float bat_a = 0.00;          // to store bat_a volt value.
float bat_b = 0.00;          // to store bat_b volt value.

// intaces
voltmeter volt;
Interface interface;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

/**
 * @brief on or off an relay
 *        0 - relay_1a. Min A
 *        1 - relay_1b. Min B
 *        2 - relay_2a. Max A
 *        3 - relay_2b. Max B
 * 
 * @param relay_index 
 */
void relay_status(byte relay_index){
  for (byte i = 0; i < 4; i++){
    digitalWrite(relays[i],LOW);
  }
  digitalWrite(relays[relay_index],HIGH);
  return;
}

/**
 * @brief comprare and check voltmeter value. Enable or disable 
 *        the relay(s) protec
 * 
 */
void battery_protec(){
  if ( bat_a > min_a && bat_a < max_a && bat_b > min_b && bat_b < max_b ){
    for (byte i = 0; i < 4; i++){
      digitalWrite(relays[i],LOW);
    }
  } else if ( bat_a < min_a && bat_b < min_b ){
      for (byte i = 0;i <2; i++){
        digitalWrite(relays[i],HIGH);
      }
  } else if ( bat_b > max_a && bat_b > max_b ){
      for (byte i = 2;i <4; i++){
        digitalWrite(relays[i],HIGH);
      }
  } else if ( bat_a < min_a && bat_b > max_b ){
      digitalWrite(relays[0],HIGH);
      digitalWrite(relays[3],HIGH);
  } else if ( bat_a > max_a && bat_b < min_b ){
      digitalWrite(relays[1],HIGH);
      digitalWrite(relays[2],HIGH);
  } else {
    if ( bat_a < min_a ){
      relay_status(0);  // call rutine for active relay 1A
    } else if ( bat_b < min_b ){
      relay_status(1);  // call rutine for  active relay 1B
    } else if ( bat_a > max_a ){
      relay_status(2);  // call rutine for active relay 2A
    } else if ( bat_b > max_b ){
      relay_status(3);  // call rutine for active relay 2B
    }
  }
  return;
}

/**
 * @brief read scope to do the instruction requset from Master
 * 
 */
void data_in(int numBytes){
  indicator = Wire.read();    // read I2C bus
  if ( indicator == 0 ) {// battery A
    /*code here*/
    data_send = String (bat_a) + '\n';
  } else if ( indicator == 1){// battery B
    /*code here*/
    data_send = String (bat_b) + '\n';
  } else if ( indicator == 2){// ip address
    /*code here*/
    data_send = ip + '\n';
  } else if ( indicator == 3){// gateway address
    /*code here*/
    data_send = gateway + '\n';
  } else if ( indicator == 4) {// netmask address 
    /*code here*/
    data_send = netmask + '\n';
  } else if ( indicator == 5){// dns address
    /*code here*/
    data_send = dns + '\n';
  }
}

/**
 * @brief 
 * 
 */
void data_out(){
  Wire.write(data_send.length());   // send size data
  //Serial.println("Size string: ");
  //Serial.println(data_send.length());
  Serial.println("Size send!");
  delay(100);
  Serial.println(data_send); // for debug
  Wire.println(data_send);
  Serial.println("----------------------");
  Serial.println("String send!");
  Wire.flush();
}

// Setup
void setup(){
  pinMode(led_debug,OUTPUT);    // led_debug pin as ouput
  pinMode(configping,INPUT);    // config pin as output
  for (byte i = 0; i < 2; i++){
    digitalWrite(led_debug,HIGH);
    delay(100);
    digitalWrite(led_debug,LOW);
    delay(100);
  }
  // init pins relay as outputs
  for (byte i = 0; i < 4; i++){
    pinMode(relays[i],OUTPUT);
    digitalWrite(relays[i],LOW);
  }

  Serial.begin(9600);           // init Serial monitor
  Serial.println("DC Voltmeter"); // print in monitor serial
  
  Wire.begin(0x0C);   // Slave with 0x0C address
  Wire.onRequest(data_out);    // recibe instrucciones desde el Master
  Wire.onReceive(data_in);   // envía los datos solicitados al Master
  analogReference(EXTERNAL);    // External reference: 4.3V
}

// Loop
void loop() {
  if ( digitalRead (configping) == HIGH ){
    Serial.println(F("Welcome to CTD config mode"));
    interface.begin();
    return;
  }
  bat_a = volt.get_volt(0); // get and save volt value to battery A
  bat_b = volt.get_volt(1); // get and save volt value to battery B
  battery_protec();
  // pritn this
  Serial.println("Voltage: ");
  Serial.print("Battery A: "); Serial.println(bat_a); // print battery A value
  Serial.print("Battery B: "); Serial.println(bat_b); // print battery B value
  Serial.println("----------------------");
  delay(1000);
}

void reading(){
  while (Serial.available() >= 0){
    if ( Serial.available() > 4 ){
      _gateway = Serial.readStringUntil('\n');
      s.trim();
      Serial.flush();
    }
  }
  delay(100);
  for (int i=0;i<4;i++){
    Serial.print(".");
    delay(10);
  }
}
/*
ip,gt,net,dns
bat a, bat b
*/





