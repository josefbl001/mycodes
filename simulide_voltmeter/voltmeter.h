/**
 * @file voltmeter.h
 * @author José .F (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2022-09-28
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef voltmeter_h
#define voltmeter_h
#include <Arduino.h>

class voltmeter{
    public:
        float get_volt(byte analog_pin);

    private:
        float _R1                = 216000.00;    // resistencia r1
        float _R2                = 9870.00;      // resistencia r2
        float _factor_division   = (_R2/(_R1+_R2)); // factor de división del divisor de tensión entre r1 y r2
        float _voltage           = 0;            // varibale para almacenar el valor calculado de la etension
        float _vbat              = 0;            // voltaje final de la batería despues de pasar por el voltimetro
        float _vref              = 4.31;         // referencia externa
        byte _readIndex = 0;              // el índice de la lectura actual
        static const byte _numReadings = 50;     // número de muestras de lectura - aumentar para un mayor suavizado
        byte _readings[_numReadings];      // la matriz para las lecturas de la entrada analógica
        unsigned long _total = 0;        // Inicializamos el total
        int   _average = 0;
        //#define alpha 1.5
};

#endif