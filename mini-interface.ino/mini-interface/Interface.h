/**
 * @file Interface.h
 * @author Jose F. (https://github.com/josefbl001)
 * @brief 
 * @version 0.1
 * @date 2022-09-05
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef Interface_h
#define Interface_h
#include <Arduino.h>
#include <EEPROM.h>

class Interface{
    public:
        void begin();
    protected:
        // ram
        String ip;             // String ip address
        String gateway;        // String gateway address
        String netmask;        // String subred address
        String dns;            // String dns address
        String bat_a1;  // set min battery a
        String bat_a2;  // set max battery a
        String bat_b1;  // set min battery b
        String bat_b2;  // set max battery b

        String str_in;
        byte byte_in;

        void principal_menu(); // show principal menu
        void store_option();   // store string input in RAM
        String read_str(byte min_size);     // to read strings from serial port
        byte read_byte(byte min_size);      // to read bytes from serial port
        void network();
        void protectors();
        void save_config();
        void store_eeprom(int addrOffset, const String &strToWrite);
        void wait_points(byte delay_time);    // show saving ram message. For avoid repea
};

#endif
// !!gasmaca