/**
 * @file main.cpp
 * @author Jose F. (https://github.com/josefbl001)
 * @brief 
 * @version 0.1
 * @date 2022-08-27
 * 
 * @see https://forum.arduino.cc/t/send-float-over-i2c/424351/5
 * @see https://forum.arduino.cc/t/sending-temperature-and-humidity-via-i2c/394904/2 
 * @see https://controlautomaticoeducacion.com/arduino/eeprom-con-arduino/
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <Arduino.h>
#include "voltmeter.h"
#include "Interface.h"
#include <EEPROM.h>

// Var for config mode
const byte configpin     = 8;      // gpio for enter to config mode 
//const byte configactive  = 13;     // gpio for indicate config mode active

// Intances
Interface interface;
// End's intances

// var to store data from eeprom
  String e_ip = "";             // String ip address
  String e_gateway = "";        // String gateway address
  String e_netmask = "";        // String subred address
  String e_dns = "";            // String dns address
  String e_bat_a1 = "";  // set min battery a
  String e_bat_a2 = "";  // set max battery a
  String e_bat_b1 = "";  // set min battery b
  String e_bat_b2 = "";  // set max battery b

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void setup() {
  Serial.begin(9600);   // init serial port
  EEPROM.get(0,e_ip);
  Serial.println(e_ip);
  int i = 0;
  pinMode(configpin,INPUT);    // Config mode pin. This pin is a jumper
  // Initial message
  Serial.println(F("Cix Telemetry Device"));
  Serial.println(F("--------------------"));
  delay(1000);
  if ( i == 1 ){
  EEPROM.get(0,e_ip);
  delay(10);
  EEPROM.get(18,e_gateway);
  delay(10);
  EEPROM.get(36,e_netmask);
  delay(10);
  EEPROM.get(54,e_dns);
  delay(10);
  EEPROM.get(72,e_bat_a1);
  delay(10);
  EEPROM.get(78,e_bat_a2);
  delay(10);
  EEPROM.get(84,e_bat_b1);
  delay(10);
  EEPROM.get(90,e_bat_b2);
  delay(10);

  Serial.println(e_ip);
  delay(50);
  Serial.println(e_gateway);
  delay(50);
  Serial.println(e_netmask);
  delay(50);
  Serial.println(e_dns);
  delay(50);
  Serial.println(e_bat_a1);
  delay(50);
  Serial.println(e_bat_a2);
  delay(50);
  Serial.println(e_bat_b1);
  delay(50);
  Serial.println(e_bat_b2);
  delay(50);
  } else {}
  delay(3000);
}

void loop() {
  if ( digitalRead(configpin) == HIGH ){
        interface.begin();    // first check if mode config is active
                          // if not, continue to the intructions
    return;
  }
  Serial.println("i'm here!");
}

// @todo funcion para guardar en eeprom