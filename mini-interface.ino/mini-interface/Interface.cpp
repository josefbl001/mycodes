/**
 * @file Interface.cpp
 * @author José F. (https://github.com/josefbl001)
 * @brief 
 * @version 0.1
 * @date 2022-09-20
 * 
 * @copyright Copyright (c) 2022
 * 
 */

// @todo "ayuda"
// @todo "guardar"

// @todo rutina para verificar que los datos que se van a guardar sean correctos
// @todo función para guardar en eeprom
// @todo mejorar la lectura en puerto serial. Debe esperar un salto de línea para leer

#include "Interface.h"

/**
 * @brief show welcome
 * 
 */
void Interface::begin(){
    Serial.println(F("~|  ¡Bienvenido a la configuración del Cix Telemetry Device!  |~"));
    principal_menu();
}

/**
 * @brief show principal menu
 * 
 */
void Interface::principal_menu(){
    Serial.println(F("Escribe el numero de la opcion que deseas configurar"));
    Serial.println(F("1- Parametros de red."));              
    Serial.println(F("2- Protectores de baterias."));
    Serial.println(F("3- Ayuda."));
    Serial.println(F("4- Guardar."));
    byte_in = read_byte(1);
    store_option();
}

/**
 * @brief compare byte input and go to funtion
 *          1-> network
 *          2-> protectors
 *          3-> show help
 *          4-> save configuration
 * 
 */
void Interface::store_option(){
    if ( byte_in == 1 ){// network configuration
        /* code here */
        network();
    } else if ( byte_in == 2 ){// protectors configuration
        /* code here */
        protectors();
    } else if ( byte_in == 3 ){// help
        /* code here */
    } else if ( byte_in == 4 ){// show current config (read from eeprom)
        /* code here */
    } else if ( byte_in == 5 ){// save new config in eeprom
        /* code here */
        save_config();
    }
}

/**
 * @brief read bytes from serial port
 * 
 */
byte Interface::read_byte(byte min_size){
    String s;
    int i;       // to store bytes input
    while (Serial.available() >= 0) {
        if ( Serial.available() > min_size){
            s = Serial.readStringUntil('\n');  // read serial buffer
            s.trim();
            Serial.flush();     // free serial buffer
            break;
        }
    }
    return i = s.toInt();
}

/**
 * @brief read string from serial port
 * 
 * @return String 
 */
String Interface::read_str(byte min_size){
    String s;   // to store string input
    while (Serial.available() >= 0) {
        if ( Serial.available() > min_size ){
            delay(2000);
            s = Serial.readStringUntil('\n');  // read string from serial buffer
            s.trim();
            Serial.flush();     // free serial buffer
            break;
        }
    }
    return s;       // return string read
}

/**
 * @brief Configuration network. Here, user enters network configuration values
 * 
 */
void Interface::network(){
    Serial.println(F("Ingresa tu direccion ip:"));      // print this
    ip = read_str(7);       // store in ip var
    wait_points(3);
    Serial.println(F("Ingresa tu puerta de enlace"));   // print this
    gateway = read_str(7);  // store in gateway var
    wait_points(3);
    Serial.println(F("Ingresa tu mascara de red"));     // print this
    netmask = read_str(7);  // store in netmask var
    wait_points(3);
    Serial.println(F("Ingresa tu direccion de nombres de dominios (DNS)"));
    dns = read_str(7);      // store in dns var
    wait_points(3);
    Serial.println(F("Regresando al menu principal..."));
    Serial.println(F(""));
    delay(2000);
    begin();
}

/**
 * @brief configuration protectors. Here, user enters protectors configuration 
 *        values
 * 
 */
void Interface::protectors(){
    Serial.println(F("Ingresa el umbral MAXIMO del protector A"));
    bat_a1 = read_str(5);
    wait_points(3);
    Serial.println(F("Ingresa el umbral MINIMO del protector A"));
    bat_a2 = read_str(5);
    wait_points(3);
    Serial.println(F("Ingresa el umbral MAXIMO del protector B"));
    bat_b1 = read_str(5);
    wait_points(3);
    Serial.println(F("Ingresa el umbral MINIMO del protector B"));
    bat_b2 = read_str(5);
    wait_points(3);
    Serial.println(F("Regresando al menu principal..."));
    Serial.println(F(""));
    delay(2000);
    begin();
}


/**
 * @brief save current config in eeprom memory
 * 
 */
void Interface::save_config(){
    String s;
    Serial.println(F("-------------------------------"));
    Serial.print("Direccion IP: ");             Serial.println(ip);
    Serial.print("Puerta de enlace: ");         Serial.println(gateway);
    Serial.print("Mascara de red: ");           Serial.println(netmask);
    Serial.print("DNS: ");                      Serial.println(dns);
    Serial.println(F("-------------------------------"));
    Serial.print("Protector de descargar A: "); Serial.println(bat_a1);
    Serial.print("Protector de carga A: ");     Serial.println(bat_a2);
    Serial.print("Protector de descarga B: ");  Serial.println(bat_b1);
    Serial.print("Protector de carga B: ");     Serial.println(bat_b2);
    Serial.println(F("-------------------------------"));
    
    Serial.println(F("Verifique su nueva configuracion antes de guardar"));
    Serial.print(F("Guardar la nueva configuracion:(S/n)?"));
    s = read_str(1);
    if ( s == "S" || s == "s" ){
        store_eeprom(0,ip);
        wait_points(10);        // wait 5 secons for save in eeprom
        Serial.println(F("Configuracion guardada!"));
        Serial.println(F(""));
        delay(1000);
    } else if ( s == "n" ){
        Serial.println("Cancelando!");
        delay(500);
        Serial.println(F("Regresando al menu principal..."));
        Serial.println(F(""));
        delay(2000);
        begin();
    } else {
        save_config();
    }
    
}

void Interface::store_eeprom(int addrOffset, const String &strToWrite){

    return;
}
/**
 * @brief 
 * 
 */
void Interface::wait_points(byte delay_time){
    Serial.println(F(""));
    Serial.print(F("Guardando"));
    delay(500);
    for (byte i = 0; i<delay_time; i++){
        Serial.print(F("."));
        delay(500);
    }
    Serial.println("");
    return;
}