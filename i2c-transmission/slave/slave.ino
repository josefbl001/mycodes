/**
* @file   slave.ino
* @author José F. ()
* @brief  
*         
*         
*
* @see https://programarfacil.com/blog/arduino-blog/comunicacion-i2c-con-arduino/
*
*/

#include <Wire.h>

//String message1 = "10.10.96.3";
float f = 10.80;    // float to send to i2c bus
String message;
bool S = false;
char r;
//-------------------------------------------------------------------
//-------------------------------------------------------------------

// setup
void setup() {
  Serial.begin(9600);   // init serial port
  Wire.begin(0x0C);     // Slave con la dirección 0x0C para el bus i2c
  Serial.println("Soy el Esclavo");
  Wire.onRequest(eventoSolicitud); // registrar evento de solicitud de datos
  Wire.onReceive(eventoRecepcion); // registrar evento de recepcion de datos
}

// loop
void loop() {
  delay(100);   // wait 100 ms
}

void eventoRecepcion(){
  if( Wire.read() == '0' ){
    S = true;
    message = String(f) + '\n';
  } 
}

void eventoSolicitud(){
  if( S == true ){
    Wire.write(message.length());
    S = false;
  }
  else{
    Wire.println(message);
  }
}
