/**
* @file   master.ino
* @author José F. ()
* @brief  Por el bus i2c, se solicita al esclavo los valores del voltimetro.
*         Estos serán enviados como un String de datos. Además, también se debe enviar
*         el valor set-point de los protectores de baterías (relays).
*
* @see https://programarfacil.com/blog/arduino-blog/comunicacion-i2c-con-arduino/
*
*/

#include <Wire.h>  // librería para el bus i2c

// vars for save value receive from slave
String ip;
String gateway;
String netmask;
String dns;

// setup
void setup() {
  Serial.begin(9600);  // init serial port
  Wire.begin();        // Master
  Serial.println("Soy el Master");
}

void loop() {
  delay(1000);
  request_data();
}

/**
* Solicitar los datos al esclavo con la direccion 0x0C
*       var       num_indicator:
*     bat-a     :       0
*     bat-b     :       1
*     ip        :       2
*     gateway   :       3
*     netmask   :       4
*     dns       :       5
*
*
*/
void request_data() {
  ip = value_request(2);
  //delay(100);
  Serial.println("---------------------------------------------");
  Serial.println("String de datos: "); Serial.println(ip);          // print this!
  Serial.println("---------------------------------------------");
  //Serial.flush();
  delay(500);  // wait for request next value...
}


String value_request(byte value){
  int i = 0;  // variable para determinar el indice donde se guardará el byte enviado
  String s;   // for store string value
  // request
  Wire.beginTransmission(0x0C);  // Comienza la transmisión con esclavo con la dirección 0x0C
  Wire.write(value);                 // solicita el valor del voltimetro A
  Wire.endTransmission();        // Fin de la transmision

  Wire.requestFrom(0x0C, 1);  // solicitar un byte al esclavo 0x0C
  byte len = Wire.read();     // no está enviando el tamaño del string
  //Serial.println(len);
  char arr[int(len)];           // se crea un arreglo del tamaño de los byte que enviará el esclavo
  //Serial.println(sizeof(arr));
  Serial.println("byte receive");
  Wire.requestFrom(0x0C, int(len));  // solicita al esclavo 0x0C la cantidad de 'len' bytes
  while (Wire.available()) {         // slave may send less than requested
    char c = Wire.read();            // receive a byte as character
    arr[i] = c;                 // guarda 'c' en el indice correspondiente
    i = i + 1;                       // +1 para el indice del array
    Serial.print(c);
  }
  //Wire.flush();
  Serial.println("");
  // for debug
  //Serial.println("---------------------------");
  //Serial.println("Data in:");
  //Serial.println(String (arr));
  //Serial.println("---------------------------");
  s = String (arr);
  return s;
}
