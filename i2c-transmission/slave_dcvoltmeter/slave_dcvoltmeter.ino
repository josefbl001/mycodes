/**
 * @file main.cpp
 * @author José F. (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2022-09-14
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <Arduino.h>
#include <Wire.h>

float R1                = 216000.00;    // resistencia r1
float R2                = 9870.00;      // resistencia r2
float factor_division   = (R2/(R1+R2)); // factor de división del divisor de tensión entre r1 y r2
float voltage           = 0;            // varibale para almacenar el valor calculado de la etension
float vbat              = 0;            // voltaje final de la batería despues de pasar por el voltimetro
float vref              = 4.31;         // referencia externa
int readIndex = 0;              // el índice de la lectura actual
const int numReadings = 50;     // número de muestras de lectura - aumentar para un mayor suavizado
int readings[numReadings];      // la matriz para las lecturas de la entrada analógica
unsigned long total = 0;        // Inicializamos el total
int   average = 0;
#define alpha 1.5

float bat_a = 0.00;          // to store bat_a volt value.
float bat_b = 0.00;          // to store bat_b volt value.
byte relay1_a = 9;    // relay battery A pin. Charge
byte relay2_a = 10;    // relay battery A pin. Discharge
byte relay1_b = 11;    // relay battery B pin. Charge
byte relay2_b = 12;    // relay battery B pin. Discharge

String data_send;     // send to i2c bus
byte indicator;       // instruction scope

String ip       = "10.10.96.100";
String gateway  = "10.10.96.1";
String netmask  = "255.255.255.0";
String dns      = "8.8.8.8";

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

/**
 * @brief 
 * 
 * @param analog_pin 
 * @return float 
 */
float voltmeter(int analog_pin){
    //Haremos esto unas 50 verces
    for (int i=0; i < numReadings ; i++) {
        // restar la última lectura:
        total = total - readings[readIndex];
        // Leer desde el sensor
        readings[readIndex] = analogRead(analog_pin);
        // añadir la lectura al total:
        total = total + readings[readIndex];
        // avanzar a la siguiente posición en el array:
        readIndex = readIndex + 1;
        // si estamos al final del array...
        if (readIndex >= numReadings) {
            // ...reiniciamos el indice:
            readIndex = 0;
        }
    }
    // calculamos el promedio:
    average = (total / numReadings);
    voltage = average * (vref / 1023.0);
    vbat    = voltage/factor_division;

    return vbat;
}

/**
 * @brief read scope to do the instruction requset from Master
 * 
 */
void data_in(int numBytes){
  indicator = Wire.read();    // read I2C bus
  if ( indicator == 0 ) {// battery A
    /*code here*/
    data_send = String (bat_a) + '\n';
  } else if ( indicator == 1){// battery B
    /*code here*/
    data_send = String (bat_b) + '\n';
  } else if ( indicator == 2){// ip address
    /*code here*/
    data_send = ip + '\n';
  } else if ( indicator == 3){// gateway address
    /*code here*/
    data_send = gateway;
  } else if ( indicator == 4) {// netmask address 
    /*code here*/
    data_send = netmask;
  } else if ( indicator == 5){// dns address
    /*code here*/
    data_send = dns;
  }
}

/**
 * @brief 
 * 
 */
void data_out(){
  Wire.write(data_send.length());   // send size data
  //Serial.println("Size string: ");
  //Serial.println(data_send.length());
  Serial.println("Size send!");
  delay(100);
  Serial.println(data_send); // for debug
  Wire.println(data_send);
  Serial.println("----------------------");
  Serial.println("String send!");
  Wire.flush();
}

// Setup
void setup() {
  Serial.begin(9600);           // init Serial monitor
  Serial.println("DC Voltmeter"); // print in monitor serial
  
  Wire.begin(0x0C);   // Slave with 0x0C addres
  Wire.onReceive(data_in);    // recibe instrucciones desde el Master
  Wire.onRequest(data_out);   // envía los datos solicitados al Master
  analogReference(EXTERNAL);    // External reference: 4.3V
}

// Loop
void loop() {
  bat_a = voltmeter(0); // get and save volt value to battery A
  bat_b = voltmeter(1); // get and save volt value to battery B
  // pritn this
  Serial.println("Voltage: ");
  Serial.print("Battery A: "); Serial.println(bat_a); // print battery A value
  Serial.print("Battery B: "); Serial.println(bat_b); // print battery B value
  Serial.println("----------------------");
  delay(1000);
}
